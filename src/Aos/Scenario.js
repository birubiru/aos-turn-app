/**
 * @typedef ScenarioArgs
 * @property {string} name name of the scenario
 * @property {boolean} hasVp whether or not the scenario is won via victory points
 * @property {string} scoredAt enum indicated when victory points are scored
 * @property {integer} maxBattleRound how long the game is played before winner decided, default is 5
 */

class Scenario {
  /**
   * 
   * @param {ScenarioArgs} args 
   */
  constructor(args) {
    const requiredFields = ["name", "hasVp"];
    for (let f of requiredFields) {
      if (!args[f]) {
        throw new Error("missing required constructor arg " + f);
      }
      this[f] = args[f];
    }
    if (this.hasVp && !args.scoredAt) {
      throw new Error("if scenario has VP scoredAt must be specified");
    }
    this.scoredAt = args.scoredAt;
    this.maxBattleRound = args.maxBattleRound || 5;
  }
}

export default Scenario;
