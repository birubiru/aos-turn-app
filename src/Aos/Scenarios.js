import Scenario from "./Scenario";
import { ScoredAt } from "./enums";

const Scenarios = {
  knifeToTheHeart: new Scenario({ name: "Knife to the heart", hasVp: false }),
  totalConquest: new Scenario({
    name: "Total Conquest",
    hasVp: true,
    scoreAt: ScoredAt.endOfTurn
  }),
  dualityOfDeath: new Scenario({
    name: "Duality of Death",
    hasVp: true,
    scoreAt: ScoredAt.endOfTurn
  }),
  battleForThePass: new Scenario({
    name: "Battle for the pass",
    hasVp: true,
    scoredAt: ScoredAt.endOfTurn
  })
};

export default Scenarios;
