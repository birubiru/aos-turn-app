import EventEmitter from "./EventEmitter";
export const AoSEvents = {
  onGameStart: "onGameStart",
  onBattleRoundStart: "onBattleRoundStart",
  onBattleRoundEnd: "onBattleRoundEnd",
  onTurnStart: "onTurnStart",
  onTurnEnd: "onTurnEnd",
  onGameEnd: "onGameEnd",
  onScoreUpdated: "onScoreUpdated"
};

class BattleRound extends EventEmitter {
  // turn order?
  turnOrder = [];
  currentTurn = 0;
  victoryPoints = [];
  // victory points scored?
  // command points earned?
  /**
   *
   * @param {string} player either p1 or p2. Will cause the onTurnStart event to be emitted
   */
  setTurnOrder(player) {
    this.turnOrder = player === "p1" ? ["p1", "p2"] : ["p2", "p1"];
    this.emit(AoSEvents.onTurnStart, player);
  }

  getCurrentPlayer() {
    return this.turnOrder[this.currentTurn];
  }

  _next() {
    // console.log("br _next");
    const afterTurnEndEvent = vpScored => {
      // console.log("vp callback");
      // TODO check if current scenario actually uses VP
      this.victoryPoints[this.currentTurn] = vpScored;
      if (this.currentTurn === 1) {
        // end of battle round
        this.emit(AoSEvents.onBattleRoundEnd);
      } else {
        this.currentTurn = 1;
        this.emit(AoSEvents.onTurnStart, this.turnOrder[this.currentTurn]);
      }
    };
    // TODO only emit callback if the scenario uses VP
    this.emit(
      AoSEvents.onTurnEnd,
      this.turnOrder[this.currentTurn],
      afterTurnEndEvent
    );
  }
}

export default class AoSGame extends EventEmitter {
  battleRounds = [];
  turns = [];
  currentBattleRound = 0;

  getCurrentBattleRound() {
    return this.battleRounds[this.currentBattleRound - 1];
  }
  getCurrentPlayerTurn() {
    return this.getCurrentBattleRound().getCurrentPlayer();
  }
  next() {
    // console.log("next");
    const nextBattleRound = () => {
      const battleRound = new BattleRound();
      battleRound.on(AoSEvents.onTurnStart, (...args) =>
        this.emit(AoSEvents.onTurnStart, ...args)
      );
      battleRound.on(AoSEvents.onBattleRoundEnd, handleBattleRoundEnd);
      battleRound.on(AoSEvents.onTurnEnd, (...args) =>
        this.emit(AoSEvents.onTurnEnd, ...args)
      );
      this.currentBattleRound += 1;
      this.battleRounds.push(battleRound);
      this.emit(AoSEvents.onBattleRoundStart, player =>
        battleRound.setTurnOrder(player)
      );
    };
    const handleBattleRoundEnd = () => {
      // check for game end
      //TODO use scenario max turns
      if (this.battleRounds.length === 5) {
        return this.emit(AoSEvents.onGameEnd);
      }
      this.emit(AoSEvents.onBattleRoundEnd);
      // create the next battle round
      nextBattleRound();
    };
    if (this.battleRounds.length === 0) {
      //start of game
      // console.log("start of game");
      nextBattleRound();
    } else {
      // console.log("next battleround");
      this.getCurrentBattleRound()._next();
    }
  }
}
