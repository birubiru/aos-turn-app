import AoS, { AoSEvents } from "./index";
import { assert } from "chai";

describe("an age of sigmar game", () => {
  let aos;
  beforeAll(() => {
    aos = new AoS();
  });
  test("game start", done => {
    aos.once(AoSEvents.onBattleRoundStart, setTurnOrderCb => {
      try {
        setTurnOrderCb("p1");
        assert.equal(aos.getCurrentBattleRound().getCurrentPlayer(), "p1");
        // cb();
      } catch (e) {
        done(e);
      }
    });
    aos.once(AoSEvents.onTurnStart, () => {
      // Turn started!
      done();
    });
    aos.next();
  });
  test("turn 1", done => {
    aos.once(AoSEvents.onTurnEnd, (player, vpCb) => {
      try {
        assert.equal(player, "p1");
        vpCb(3);
      } catch (e) {
        done(e);
      }
    });

    aos.once(AoSEvents.onTurnStart, player => {
      try {
        assert.equal(player, "p2");
      } catch (e) {
        done(e);
      }

      done();
    });
    try {
      assert.equal(aos.getCurrentBattleRound().getCurrentPlayer(), "p1");
      aos.next();
      // done();
    } catch (e) {
      done(e);
    }
    // aos.next();
  });
  test("turn 1.2", done => {
    aos.once(AoSEvents.onTurnEnd, (player, vpCb) => {
      try {
        assert.equal(player, "p2");
        vpCb(1);
      } catch (e) {
        done(e);
      }
    });
    aos.once(AoSEvents.onBattleRoundEnd, () => {});
    aos.once(AoSEvents.onBattleRoundStart, setPlayerOrderCb => {
      setPlayerOrderCb("p2");
      try {
        assert.equal(aos.getCurrentPlayerTurn(), "p2");
        done();
      } catch (e) {
        done(e);
      }
    });
    aos.next();
  });
  test("endOfGame", done => {
    aos.on(AoSEvents.onTurnEnd, (player, vpCb) => {
      vpCb(1);
    });
    aos.on(AoSEvents.onBattleRoundStart, setPlayerOrderCb => {
      setPlayerOrderCb("p1");
    });
    aos.on(AoSEvents.onGameEnd, () => {
      done();
    });
    aos.next(); //end of turn 2.1
    aos.next(); //end of turn 2.2
    aos.next(); //end of turn 3.1
    aos.next(); //end of turn 3.2
    aos.next(); //end of turn 4.1
    aos.next(); //end of turn 4.2
    aos.next(); //end of turn 5.1
    aos.next(); //end of turn 5.2
  });
});
