export default class EventEmitter {
  eventHandlers = {};
  allHandlers = {};
  handlerIdCounter = 0;

  _initializeHandlers(eventName) {
    if (!this.eventHandlers[eventName]) this.eventHandlers[eventName] = [];
  }

  _getHandlerId() {
    const i = this.handlerIdCounter;
    this.handlerIdCounter += 1;
    return i;
  }

  _addHandler(eventName, handler, isOnce = false) {
    this._initializeHandlers(eventName);
    const id = this._getHandlerId();
    this.allHandlers[id] = { handler, isOnce };
    this.eventHandlers[eventName].push(id);
  }

  on(eventName, handler) {
    this._addHandler(eventName, handler);
  }
  once(eventName, handler) {
    this._addHandler(eventName, handler, true);
  }
  emit(eventName, ...args) {
    if (!this.eventHandlers[eventName]) return;
    const handlersToRun = [...this.eventHandlers[eventName]];
    handlersToRun.forEach((handlerId, index) => {
      const handler = this.allHandlers[handlerId];
      handler.handler(...args);
      if (handler.isOnce) {
        delete this.allHandlers[handlerId];
        this.eventHandlers[eventName].splice(index, 1);
      }
    });
  }
}
