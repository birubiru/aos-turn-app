export function capitalizeWord(word) {
  const parts = word.split("");
  parts[0] = parts[0].toUpperCase();
  return parts.join("");
}

export function className(classNames) {
  let finalClassName = [];
  for (let className of classNames) {
    if (typeof className === typeof "") {
      finalClassName.push(className);
    } else {
      // assume object
      const name = className.name;
      if (className.conditional) {
        if (className.conditional()) finalClassName.push(name);
      } else {
        finalClassName.push(name);
      }
    }
  }
  return finalClassName.join(" ").trim();
}
