import React, { Component } from "react";
import { capitalizeWord, className } from "../utils";
import {
  ScoreKeeper,
  AddTrackerModal,
  Button,
  PromptTurnOrderModal
} from "./components";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, this._getInitialState(), {
      swapSides: false
    });
  }
  render() {
    const {
      battleRound,
      currentPlayerTurn,
      turn,
      redCommandPoints,
      redVictoryPoints,
      blueCommandPoints,
      blueVictoryPoints,
      swapSides
    } = this.state;
    const teamSections = [
      <TeamSection
        team="blue"
        key="blue"
        victoryPoints={blueVictoryPoints}
        commandPoints={blueCommandPoints}
        onModifyVictoryPoints={m =>
          this.setState({
            blueVictoryPoints: modifyOrMinMax(blueVictoryPoints, m)
          })
        }
        onModifyCommandPoints={m =>
          this.setState({
            blueCommandPoints: modifyOrMinMax(blueCommandPoints, m)
          })
        }
        onShowAddTracker={() => this._onShowAddTracker("blue")}
        additionalTrackers={this.state.blueAdditionalTrackers}
        onModifyAdditionalTracker={(team, trackerId, value) =>
          this._onAdditionalTrackerChange(team, trackerId, value)
        }
      />,
      <TeamSection
        team="red"
        key="red"
        victoryPoints={redVictoryPoints}
        commandPoints={redCommandPoints}
        onModifyVictoryPoints={m =>
          this.setState({
            redVictoryPoints: modifyOrMinMax(redVictoryPoints, m)
          })
        }
        onModifyCommandPoints={m =>
          this.setState({
            redCommandPoints: modifyOrMinMax(redCommandPoints, m)
          })
        }
        onShowAddTracker={() => this._onShowAddTracker("red")}
        additionalTrackers={this.state.redAdditionalTrackers}
        onModifyAdditionalTracker={(team, trackerId, modifier) =>
          this._onAdditionalTrackerChange(team, trackerId, modifier)
        }
      />
    ];
    if (swapSides) {
      teamSections.reverse();
    }
    return (
      <div className="App column">
        <Header
          playerTurns={this.state.playerTurns}
          battleRound={battleRound}
          currentPlayerTurn={currentPlayerTurn}
          turn={turn}
          onTurnChange={m => this.setState({ turn: modifyOrMinMax(turn, m) })}
          onSwapSides={() => this.setState({ swapSides: !swapSides })}
          onReset={() => this.setState(this._getInitialState())}
          onNext={() => this._onNextTurn()}
        />
        <div className="row grow-1">{teamSections}</div>
        <AddTrackerModal
          show={this.state.showAddTrackerModal}
          onClose={tracker => this._onAddTrackerClose(tracker)}
        ></AddTrackerModal>
        <PromptTurnOrderModal
          show={this.state.showPromptTurnOrderModal}
          onClose={player => {
            const newState = { showPromptTurnOrderModal: false };
            if (player) {
              newState.playerTurns = [
                ...this.state.playerTurns,
                player === "red" ? ["red", "blue"] : ["blue", "red"]
              ];
              newState.currentPlayerTurn = player;
              newState.battleRound = this.state.battleRound + 1;
              newState.turn = 0;
            }
            this.setState(newState);
          }}
        ></PromptTurnOrderModal>
      </div>
    );
  }

  async _onNextTurn() {
    const { battleRound, turn, currentPlayerTurn } = this.state;
    if (battleRound === 0 || turn === 1) {
      this.setState({ showPromptTurnOrderModal: true });
      return;
    }
    if (turn === 0) {
      this.setState({
        turn: 1,
        currentPlayerTurn: currentPlayerTurn === "red" ? "blue" : "red"
      });
      return;
    }
    throw new Error("whoops?");
  }

  _onAdditionalTrackerChange(team, trackerId, modifier) {
    const trackerKey = `${team}AdditionalTrackers`;
    const trackersToSave = [];
    const stateToSet = {};
    for (let tracker of this.state[trackerKey]) {
      tracker = { ...tracker };
      if (tracker.id === trackerId) {
        tracker.value = modifyOrMinMax(tracker.value, modifier);
      }
      trackersToSave.push(tracker);
    }
    stateToSet[trackerKey] = trackersToSave;
    this.setState(stateToSet);
  }

  _onAddTrackerClose(tracker) {
    const stateToSet = { showAddTrackerModal: false };
    if (tracker) {
      const trackerKey = `${this.state.showAddTrackerModal}AdditionalTrackers`;
      const id = this.state.nextTrackerId;
      stateToSet.nextTrackerId = id + 1;
      tracker.id = id;
      tracker.value = 0;
      stateToSet[trackerKey] = [...this.state[trackerKey], tracker];
    }
    this.setState(stateToSet);
  }

  _onShowAddTracker(teamName) {
    this.setState({ showAddTrackerModal: teamName });
  }

  _handleTurnModify(v) {
    const { turn } = this.state;
    const newValue = turn + v;
    if (newValue < 0) {
      return;
    }
    this.setState({ turn: newValue });
  }

  _getInitialState() {
    return {
      battleRound: 0,
      turn: 0,
      playerTurns: [],
      currentPlayerTurn: null,
      redCommandPoints: 0,
      redVictoryPoints: 0,
      blueCommandPoints: 0,
      blueVictoryPoints: 0,
      redAdditionalTrackers: [],
      blueAdditionalTrackers: [],
      showAddTrackerModal: false,
      showPromptTurnOrderModal: false,
      nextTrackerId: 0
    };
  }
}

function Header(props) {
  const {
    battleRound,
    currentPlayerTurn,
    playerTurns,
    turn,
    onTurnChange,
    onReset,
    onSwapSides,
    onNext
  } = props;
  const started = battleRound > 0;
  console.log("turn", turn, "battleround", battleRound);
  return (
    <div className="row header">
      <div className="column centered">
        <div className="button" onClick={onSwapSides}>
          Swap sides
        </div>
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <div>Battle Round</div>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div className="br-container">
            <div
              className={className([
                "br-turn-container",
                {
                  name: "br-current",
                  conditional: () => battleRound === 1
                }
              ])}
            >
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 1 && turn === 0
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[0] && playerTurns[0][0] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[0] && playerTurns[0][0] === "blue"
                  }
                ])}
              ></div>
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 1 && turn === 1
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[0] && playerTurns[0][1] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[0] && playerTurns[0][1] === "blue"
                  }
                ])}
              ></div>
            </div>
            <div className="br-turn-container">
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 2 && turn === 0
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[1] && playerTurns[1][0] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[1] && playerTurns[1][0] === "blue"
                  }
                ])}
              ></div>
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 2 && turn === 1
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[1] && playerTurns[1][1] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[1] && playerTurns[1][1] === "blue"
                  }
                ])}
              ></div>
            </div>
            <div className="br-turn-container">
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 3 && turn === 0
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[2] && playerTurns[2][0] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[2] && playerTurns[2][0] === "blue"
                  }
                ])}
              ></div>
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 3 && turn === 1
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[2] && playerTurns[2][1] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[2] && playerTurns[2][1] === "blue"
                  }
                ])}
              ></div>
            </div>
            <div className="br-turn-container">
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 4 && turn === 0
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[3] && playerTurns[3][0] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[3] && playerTurns[3][0] === "blue"
                  }
                ])}
              ></div>
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 4 && turn === 1
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[3] && playerTurns[3][1] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[3] && playerTurns[3][1] === "blue"
                  }
                ])}
              ></div>
            </div>
            <div className="br-turn-container">
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 5 && turn === 0
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[4] && playerTurns[4][0] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[4] && playerTurns[4][0] === "blue"
                  }
                ])}
              ></div>
              <div
                className={className([
                  "br-turn",
                  {
                    name: "br-current-turn",
                    conditional: () => battleRound === 5 && turn === 1
                  },
                  {
                    name: "br-turn-red",
                    conditional: () =>
                      playerTurns[4] && playerTurns[4][1] === "red"
                  },
                  {
                    name: "br-turn-blue",
                    conditional: () =>
                      playerTurns[4] && playerTurns[4][1] === "blue"
                  }
                ])}
              ></div>
            </div>
            <div onClick={onNext}>{started ? "Next" : "Start"}</div>
            {/* <div>Battle Round {battleRound}</div>
        <div>{started ? currentPlayerTurn : undefined}</div>
        <div onClick={onNext}>{started ? "Next" : "Start"}</div> */}
          </div>
        </div>
      </div>
      {/* <ScoreKeeper
        value={turn}
        title="Battle Round"
        modifiers={[1]}
        onChange={onTurnChange}
      /> */}
      <div className="column centered">
        <div className="button" onClick={onReset}>
          Reset
        </div>
      </div>
    </div>
  );
}

function TeamSection(props) {
  const {
    team,
    victoryPoints,
    commandPoints,
    onModifyVictoryPoints,
    onModifyCommandPoints,
    onShowAddTracker,
    additionalTrackers,
    onModifyAdditionalTracker
  } = props;
  if (!(team === "blue" || team === "red")) {
    throw new Error("invalid team");
  }

  return (
    <div className={`grow-1 team-container ${team}-team-container`}>
      <div>{`${capitalizeWord(team)} Team`}</div>
      <ScoreKeeper
        title="Victory Points"
        modifiers={[1, 2, 3]}
        value={victoryPoints}
        onChange={onModifyVictoryPoints}
      />
      <ScoreKeeper
        title="Command Points"
        modifiers={[1]}
        onChange={onModifyCommandPoints}
        value={commandPoints}
      />
      {additionalTrackers.length > 0
        ? additionalTrackers.map(t => (
            <ScoreKeeper
              key={t.id.toString(10)}
              title={t.name}
              modifiers={t.modifiers}
              onChange={m => onModifyAdditionalTracker(team, t.id, m)}
              value={t.value}
            ></ScoreKeeper>
          ))
        : undefined}
      <Button className="add-tracker" onClick={onShowAddTracker}>
        Add Tracker
      </Button>
    </div>
  );
}

function modifyOrMinMax(v, m) {
  const min = 0;
  const max = 99;
  return v + m < min ? min : v + m > max ? max : v + m;
}

export default App;
