import React from "react";

export default function scoreKeeper(props) {
  const { value, onChange, modifiers, title } = props;
  const negativeModifiers = modifiers.map(m => m * -1);
  negativeModifiers.sort(reverseSort);
  modifiers.sort();
  return (
    <div className={"column"}>
      <div className="score-keeper-title">{title}</div>
      <div className={"row score-keeper"}>
        {negativeModifiers.map((v, i) => (
          <ScoreKeeperButton onClick={() => onChange(v)} key={i.toString(10)}>
            {v}
          </ScoreKeeperButton>
        ))}
        <div className="value">{value}</div>
        {modifiers.map((v, i) => (
          <ScoreKeeperButton key={i.toString()} onClick={() => onChange(v)}>
            +{v}
          </ScoreKeeperButton>
        ))}
      </div>
    </div>
  );
}

function ScoreKeeperButton(props) {
  const { onClick, children } = props;
  return (
    <div onClick={onClick} className="button score-keeper-button">
      {children}
    </div>
  );
}

function reverseSort(a, b) {
  a = Math.abs(a);
  b = Math.abs(b);
  if (a > b) {
    return -1;
  } else if (a < b) {
    return 1;
  }
  return 0;
}
