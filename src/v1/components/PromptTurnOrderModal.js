import React from "react";
import "./PromptTurnOrderModal.css";
import Button from "./Button";

export default class PromptTurnOrderModal extends React.Component {
  render() {
    const show = this.props.show ? true : false;
    const onClose = this.props.onClose ? this.props.onClose : () => {};
    return (
      <div className="modal" style={{ display: show ? "block" : "none" }}>
        <div className="modal-content">
          <div className="modal-header">
            <span
              className="close"
              onClick={() => {
                this._resetState();
                onClose();
              }}
            >
              &times;
            </span>
            <h2>Who won the roll off?</h2>
          </div>
          <div className="modal-body">
            <Button onClick={() => onClose("red")}>Red</Button>
            <Button onClick={() => onClose("blue")}>Blue</Button>
          </div>
        </div>
      </div>
    );
  }
}
