import React from "react";
import "./Button.css";

export default class Button extends React.Component {
  render() {
    const props = this.props;
    let classNames = ["btn"];
    if (props.className) {
      classNames.push(...props.className.split(/\s+/));
    }
    return (
      <div className={classNames.join(" ")} onClick={props.onClick}>
        {props.children}
      </div>
    );
  }
}
