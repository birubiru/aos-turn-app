import React from "react";
import "./AddTrackerModal.css";
import Button from "./Button";

export default class AddTrackerModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trackerName: ""
    };
  }
  render() {
    const show = this.props.show ? true : false;
    const onClose = this.props.onClose ? this.props.onClose : () => {};
    return (
      <div className="modal" style={{ display: show ? "block" : "none" }}>
        <div className="modal-content">
          <div className="modal-header">
            <span
              className="close"
              onClick={() => {
                this._resetState();
                onClose();
              }}
            >
              &times;
            </span>
            <h2>Add Tracker</h2>
          </div>
          <div className="modal-body">
            <input
              placeholder="Tracker Name"
              value={this.state.trackerName}
              onChange={e => this.setState({ trackerName: e.target.value })}
            ></input>
          </div>
          <div className="modal-footer">
            <Button
              onClick={() => {
                if (this.state.trackerName) {
                  const tracker = {
                    name: this.state.trackerName,
                    modifiers: [1]
                  };
                  this._resetState();
                  onClose(tracker);
                }
              }}
            >
              Add Tracker
            </Button>
          </div>
        </div>
      </div>
    );
  }

  _resetState() {
    this.setState({ trackerName: "" });
  }
}
